<?php
/*
*
	Plugin Name: Review Box
	Description: Adds a review box to all posts allowing scene articles to show a pretty review box at the beginning of the articles
	Author: Chris King
	Author URI: cjk508.com
 */


add_action('init', 'ckReviewBox_Review_init');
function ckReviewBox_Review_init(){

	add_action('add_meta_boxes', 'ckReviewBox_Review_metaBox' );
	add_action('admin_enqueue_scripts', 'ckReviewBox_Scripts');
	add_action('save_post', 'ckReviewBox_save_postdata',1 );

	add_action( 'wp_ajax_get_embed', 'get_embed_callback' );

	add_action( 'admin_footer', 'ckReviewBox_javascript' );
	add_filter('the_content', 'filter_ckReviewBox_review');
}

function ckReviewBox_Review_metaBox(){
	add_meta_box('Scene_Review', 'Scene Review','ckReviewBox_Review_Custom_Meta', 'post','normal', 'high');
}

function ckReviewBox_Review_Custom_Meta()
{
	global $post;


	echo '<input type="hidden" name="ckReviewBox_Review_noncename" id = "ckReviewBox_Review_noncename" value ='.
	wp_create_nonce(plugin_basename(__FILE__)).'"/>';

	ckReviewBox_createTextBox("reviewName","Heading", $post);
	ckReviewBox_createTextBox("reviewSubname","Subheading", $post);
	ckReviewBox_createTextBox("reviewURL","Review Link", $post);
	ckReviewBox_createRatings($post);

	ckReviewBox_createAlbumArt($post);

	ckReviewBox_createEmbed($post);
}

function ckReviewBox_Scripts(){
	if ( ! did_action( 'wp_enqueue_media' ) )
    	wp_enqueue_media();

	wp_register_script( 'uploader-js', plugins_url( 'uploader.js' , __FILE__ ), array('jquery'),'4.6.1',true );
	wp_enqueue_script( 'uploader-js' );
}

function ckReviewBox_createEmbed($post){
	$embedID =  htmlspecialchars(get_post_meta($post->ID, 'reviewoEmbed', true));
	$field_name = 'reviewoEmbed';
	echo		'<p><label for="reviewoEmbed">Add Embeddable Content</label></p>
				<input type="url" class="rwmb-oembed" name="'.$field_name.'" id="'.$field_name.'" value="'.$embedID.'">
				<input type = "button" id = "show-embed" class="show-embed button" value ="'.__( 'Preview', 'meta-box' ).'" />
				<span class="spinner"></span>
				<div class="embed-code" id ="embed-code" >'.($embedID ? get_embed( $embedID ) : '').'</div>';
		?>
	<?php
}

function ckReviewBox_createTextBox($forID, $Text, $post){
	$label = '<p><label for="'.$forID.'">'.$Text.'</label>';
	$textBox = '<input class = "widefat" type = "text" name = "'.$forID.'" value = "'. sanitize_text_field(get_post_meta($post->ID, $forID, true)).'" /> </p>';
	echo $label . $textBox;
}

function ckReviewBox_createAlbumArt($post){?>
	<p>
		<label for="review-image-url">Background Image</label>
		<p>
	  		<img class = "nothing" id ="nothing" src ="<?php echo  sanitize_text_field(get_post_meta($post->ID, 'review-image-url', true)); ?>" height = "200px"/>
	  	</p>
	</p>
	  <p>
	  <input id="ckReviewBox_upload_image" type="hidden" name="ckReviewBox_upload_image" value = "<?php echo  sanitize_text_field(get_post_meta($post->ID, 'ckReviewBox_upload_image', true)); ?>" />
	  <input id="image-url" type="text" name="review-image-url" value = "<?php echo  sanitize_text_field(get_post_meta($post->ID, 'review-image-url', true)); ?>"/>
	  <input id="upload-button" type="button" class="button" value="Upload Image" />
	 </p>
<?php
}


function ckReviewBox_createRatings($post){
	echo '<p><label for="reviewRatings">Review Rating</label>';

	$selected = get_post_meta($post->ID, 'reviewRatings', true);
	echo '<select name = "reviewRatings" id = "reviewRatings">';
	foreach (ckReviewBox_Ratings() as $option) {
		$label = $option['label'];
		$value = $option['value'];

		echo '<option value = "'.$value.'" ' . selected($selected, $value) . '>'.$label.'</option>';

	}
	echo '</select></p>';
}

function ckReviewBox_Ratings(){
	return array(
		1 => array(
			'label' => __('--'),
			'value' => 1,
			'output' => ''
			),
		2 => array(
			'label' => __('0.5 Star'),
			'value' => 2,
			'output' => '<span class="glyphicon glyphicon-star half" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span itemprop="ratingValue">0.5</span>/<span itemprop="bestRating">5</span>'
			),
		3 => array(
			'label' => __('1 Star'),
			'value' => 3,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span itemprop="ratingValue">1</span>/<span itemprop="bestRating">5</span>'
			),
		4 => array(
			'label' => __('1.5 Stars'),
			'value' => 4,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star half" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span itemprop="ratingValue">1.5</span>/<span itemprop="bestRating">5</span>'
			),
		5 => array(
			'label' => __('2 Stars'),
			'value' => 5,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span itemprop="ratingValue">2</span>/<span itemprop="bestRating">5</span>'
			),
		6 => array(
			'label' => __('2.5 Stars'),
			'value' => 6,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star half" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span itemprop="ratingValue">2.5</span>/<span itemprop="bestRating">5</span>'
			),
		7 => array(
			'label' => __('3 Stars'),
			'value' => 7,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span itemprop="ratingValue">3</span>/<span itemprop="bestRating">5</span>'
			),
		8 => array(
			'label' => __('3.5 Stars'),
			'value' => 8,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star half" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span itemprop="ratingValue">3.5</span>/<span itemprop="bestRating">5</span>'
			),
		9 => array(
			'label' => __('4 Stars'),
			'value' => 9,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
						<span itemprop="ratingValue">4</span>/<span itemprop="bestRating">5</span>'
			),
		10 => array(
			'label' => __('4.5 Stars'),
			'value' => 10,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star half" aria-hidden="true"></span>
						<span itemprop="ratingValue">4.5</span>/<span itemprop="bestRating">5</span>'
			),
		11 => array(
			'label' => __('5 Stars'),
			'value' => 11,
			'output' => '<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						<span itemprop="ratingValue">5</span>/<span itemprop="bestRating">5</span>'
			)
		);
}

function ckReviewBox_save_postdata($post_id){
		// Checks save status
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'yv_nonce' ] ) && wp_verify_nonce( $_POST[ 'yv_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

	// Exits script depending on save status
	if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
	    return;
	}

	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'reviewName' ] ) ) {
		$data = sanitize_text_field( $_POST[ 'reviewName' ] );
		if ($data != ""){
			update_post_meta( $post_id, 'reviewName', $data  );
		}
		else{
			delete_post_meta( $post_id, 'reviewName' );
		}
	}
	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'reviewSubname' ] ) ) {
		$data =sanitize_text_field( $_POST[ 'reviewSubname' ] ) ;
	    update_post_meta( $post_id, 'reviewSubname', $data);
	    if ($data != ""){
	    	update_post_meta( $post_id, 'reviewSubname', $data  );
	    }
	    else{
	    	delete_post_meta( $post_id, 'reviewSubname' );
	    }
	}
	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'reviewURL' ] ) ) {
		$data =htmlspecialchars( $_POST[ 'reviewURL' ] ) ;
	    update_post_meta( $post_id, 'reviewURL', $data);
	    if ($data != ""){
	    	update_post_meta( $post_id, 'reviewURL', $data  );
	    }
	    else{
	    	delete_post_meta( $post_id, 'reviewURL' );
	    }
	}
	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'reviewoEmbed' ] ) ) {
		$data =htmlspecialchars( $_POST[ 'reviewoEmbed' ] ) ;
	    update_post_meta( $post_id, 'reviewoEmbed', $data);
	    if ($data != ""){
	    	update_post_meta( $post_id, 'reviewoEmbed', $data  );
	    }
	    else{
	    	delete_post_meta( $post_id, 'reviewoEmbed' );
	    }
	}
	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'ckReviewBox_upload_image' ] ) ) {
		$data =sanitize_text_field( $_POST[ 'ckReviewBox_upload_image' ] ) ;
	    update_post_meta( $post_id, 'ckReviewBox_upload_image', $data);
	    if ($data != ""){
	    	update_post_meta( $post_id, 'ckReviewBox_upload_image', $data  );
	    }
	    else{
	    	delete_post_meta( $post_id, 'ckReviewBox_upload_image' );
	    }
	}
	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'review-image-url' ] ) ) {
		$data =htmlspecialchars( $_POST[ 'review-image-url' ] ) ;
	    update_post_meta( $post_id, 'review-image-url', $data);
	    if ($data != ""){
	    	update_post_meta( $post_id, 'review-image-url', $data  );
	    }
	    else{
	    	delete_post_meta( $post_id, 'review-image-url' );
	    }
	}
	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'reviewRatings' ] ) ) {
		$data =wp_kses_data( $_POST[ 'reviewRatings' ] );
	    update_post_meta( $post_id, 'reviewRatings', $data );
	    $rating_array = ckReviewBox_Ratings();
	    $dataCheck = $rating_array[$data]['value'];
	    if ($dataCheck != 1){
	    	update_post_meta( $post_id, 'reviewRatings', $data  );
	    }
	    else{
	    	delete_post_meta( $post_id, 'reviewRatings' );
	    }
	}
}

function filter_ckReviewBox_review($content){
	global $post;
	$reviewName = null;
	$reviewSubname = null;
	$reviewURL = null;
	$upload_image = null;
	$reviewRatings = null;

	$reviewFound = false;
	if(get_post_meta($post->ID, 'reviewName', true)){
		$reviewFound = true;
		$reviewName = get_post_meta($post->ID, 'reviewName', true);
		if( get_post_meta($post->ID, 'reviewURL', true)){
			$reviewURL = get_post_meta($post->ID, 'reviewURL', true);
			$reviewName = '<h1><a href="'.$reviewURL.'" target="_blank">'.$reviewName.'</a></h1>';
		}
		else{
			$reviewName = '<h1>'.$reviewName.'</h1>';
		}
	}
	else{
		if( get_post_meta($post->ID, 'reviewURL', true)){
			$reviewFound = true;
			$reviewURL = get_post_meta($post->ID, 'reviewURL', true);
			$reviewName = getTitle($reviewURL);
			$reviewName = '<h1><a href="'.$reviewURL.'" target="_blank">'.$reviewName.'</a></h1>';
		}
	}
	if( get_post_meta($post->ID, 'reviewSubname', true)){
		$reviewFound = true;
		$reviewSubname = get_post_meta($post->ID, 'reviewSubname', true);
		$reviewSubname = '<h2>'.$reviewSubname.'</h2>';
	}
	if (get_post_meta($post->ID, 'ckReviewBox_upload_image', true)){
		$reviewFound = true;
		$thumbnailID = get_post_meta($post->ID, 'ckReviewBox_upload_image', true);
		$upload_images = wp_get_attachment_metadata($thumbnailID);
		$URL = 'http://www.yorkvision.co.uk/wp-content/uploads/';
		$dates = explode ( "/", $upload_images['file']);
		try{
			$testImage = $upload_images['sizes']['review-Background']['file'];
			if ($testImage == ""){
				$testImage = $upload_images['sizes']['large']['file'];
			}
		}
		catch (Exception $e){
			$testImage = $upload_images['sizes']['large']['file'];
		}
		$upload_image = $URL.$dates[0].'/'.$dates[1].'/'.$testImage;
	}
	if( get_post_meta($post->ID, 'reviewRatings', true)){

		$reviewRatings = get_post_meta($post->ID, 'reviewRatings', true);

		$rating_array = ckReviewBox_Ratings();
		$reviewFound = true;
		$reviewRatings = '<span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><h3>Rating:'.$rating_array[$reviewRatings]['output'].'</h3></span>';
	}
	if( get_post_meta($post->ID, 'reviewoEmbed', true)){

		$reviewoEmbed = get_post_meta($post->ID, 'reviewoEmbed', true);

		$reviewoEmbed = wp_oembed_get( $reviewoEmbed);
	}

	if($reviewFound){
		$reviewBox = '
		<style>
		.jumbotron{
			background-image: url('.$upload_image.');
			background-color: rgb(0,0,0);
			background-repeat: no-repeat;
			background-size:contain;
			background-position: center;
			width: 100%;
			border-radius: 6px;
			padding: 48px 60px 56.25% 60px
  			min-height: 200px;
			position: relative;
			margin-bottom: 30px;
    		color: inherit;
		}
		.informationBox {
			  padding-bottom: 30px !important;
			  position: absolute;
			  bottom: 0px;
			  left: 0px;
			  background: rgba(0,0,0,0.5);
			  color: white;
			  width: 100%;
			  padding: 0px 10px;
		}
		.informationBox a {
			color:white;
		}
		.informationBox h1, .informationBox h1 a{
			font-size: 1.5em;
			font-weight: 600;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		.informationBox h2{
			font-family: Verdana,Geneva,sans-serif;
			font-size: 1.2em;
			font-weight: 300;
			padding-top: 10px;
		}
		.glyphicon-star.half {
		  position: relative;
		}
		.glyphicon-star.half:before {
		  position: relative;
		  z-index: 9;
		  width: 47%;
		  display: block;
		  overflow: hidden;
		}
		.glyphicon-star.half:after {
		  content: "\e006";
		  position: absolute;
		  z-index: 8;
		  color: #bdc3c7;
		  top: 0;
		  left: 0;
		}


		</style>
		<div class="jumbotron">
		  <div class="container">
			  <div class="informationBox">'.$reviewName.$reviewSubname.$reviewRatings.'
			  </div>
		  </div>
		</div>
		';

		return $reviewBox.$content.$reviewoEmbed;
	}
	else{
		return $content;
	}


}

function getTitle($Url){
    $str = file_get_contents($Url);
    if(strlen($str)>0){
        preg_match("/\<title\>(.*)\<\/title\>/",$str,$title);
        return $title[1];
    }
}

function get_attachment_id_by_url( $url ) {
    $post_id = attachment_url_to_postid( $url );

    if ( ! $id ){
        $dir = wp_upload_dir();
        $path = $url;
        if ( 0 === strpos( $path, $dir['baseurl'] . '/' ) ) {
            $path = substr( $path, strlen( $dir['baseurl'] . '/' ) );
        }

        if ( preg_match( '/^(.*)(\-\d*x\d*)(\.\w{1,})/i', $path, $matches ) ){
            $url = $dir['baseurl'] . '/' . $matches[1] . $matches[3];
            $post_id = attachment_url_to_postid( $url );
        }
    }

    return (int) $post_id;
}


/**
 * Ajax callback for returning oEmbed HTML
 *
 * @return void
 */
function get_embed_callback()
{
	$url = isset( $_POST['reviewoEmbed'] ) ? $_POST['reviewoEmbed'] : 'TEst';
	wp_send_json_success( get_embed( $url ) );
}

function get_embed( $url )
{
	$embed = wp_oembed_get( $url , array('width'=>400) );

	return $embed ? $embed : __( 'Embed HTML not available.', 'meta-box' );
}

function ckReviewBox_javascript(){ ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {
		$('#show-embed').click(function(e){
			$url= $('#reviewoEmbed').val();
			var data = {
				'action': 'get_embed',
				reviewoEmbed: $url
			};
			// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
			$.post(ajaxurl, data, function(response) {
				$('#embed-code').html(response.data);
			});
		});
	});
	</script> <?php
}


?>
