jQuery(document).ready(function($){

  var mediaUploader;

 $('#nothing').replaceWith("<img id = 'nothing' src='"+$('#image-url').val()+"' height = '200px' width = 'auto' />");
  $('#upload-button').click(function(e) {
    e.preventDefault();
    // If the uploader object has already been created, reopen the dialog
      if (mediaUploader) {
      mediaUploader.open();
      return;
    }
    // Extend the wp.media object
    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: 'Choose Image',
      button: {
      text: 'Choose Image'
    }, multiple: false });

    // When a file is selected, grab the URL and set it as the text field's value
    mediaUploader.on('select', function() {
      attachment = mediaUploader.state().get('selection').first().toJSON();
      $('#nothing').replaceWith("<img id = 'nothing' src='"+attachment.url+"' height = '200px' width = 'auto' />");
      $('#image-url').val(attachment.url);
      console.log(attachment.url);
      console.log(attachment.id);
      $('#yorkvisi_upload_image').val(attachment.id);
    });
    // Open the uploader dialog
    mediaUploader.open();
  });

});